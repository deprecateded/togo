package com.example.filipe.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import com.example.filipe.myapplication.task.TaskDAO;

import java.util.ArrayList;
import java.util.List;

public class TaskAdapter extends ArrayAdapter<Task>
{

    private Context mContext;
    private List<Task> taskList = new ArrayList<>();


    public TaskAdapter(@NonNull Context context, ArrayList<Task> list) {
        super(context, 0 , list);
        mContext = context;
        taskList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            listItem = LayoutInflater.from(getContext()).inflate(R.layout.item_todo, parent, false);
        }
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_todo,parent,false);

        View taskLine = (View) listItem.findViewById(R.id.task);

        final Task currentTask = taskList.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.task_title);
        name.setText(currentTask.getTitle());

        final Chronometer chronometer = (Chronometer) listItem.findViewById(R.id.task_duration);

        final Button button = (Button) listItem.findViewById(R.id.task_toggle);

        taskLine.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TaskActivity.class);
                intent.putExtra("task", currentTask);
                mContext.startActivity(intent);
            }

        });

        chronometer.setBase(SystemClock.elapsedRealtime() - ((long) currentTask.getTodayMilliseconds()));

        if (currentTask.isRunning()) {
            button.setText("Parar");
            chronometer.start();
        }

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                currentTask.toggle();
                if (currentTask.isRunning()) {
                    button.setText("Parar");
                    chronometer.setBase(SystemClock.elapsedRealtime() - ((long) currentTask.getTodayMilliseconds()));
                    chronometer.start();
                } else {
                    currentTask.setMilliseconds(SystemClock.elapsedRealtime() - chronometer.getBase());
                    button.setText("Iniciar");
                    chronometer.stop();
                }
                TaskDAO.update(currentTask);
                notifyDataSetChanged();
            }

        });

        return listItem;
    }


}