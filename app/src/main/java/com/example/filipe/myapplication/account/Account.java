package com.example.filipe.myapplication.account;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class Account {
    static GoogleSignInAccount account;

    public static void setAccount(GoogleSignInAccount a) {
        account = a;
    }

    public static GoogleSignInAccount getAccount() {
        return account;
    }
}
