package com.example.filipe.myapplication;

import android.content.Context;

import java.util.List;

import me.everything.providers.android.calendar.Calendar;
import me.everything.providers.android.calendar.CalendarProvider;
import me.everything.providers.android.calendar.Event;

public class CalendarManager {

    private Context context;
    private CalendarProvider calendarProvider;

    public CalendarManager(Context context){
        this.context = context;
        this.calendarProvider = new CalendarProvider(context);
    }

    public List<Calendar> getCalendarList(){
        List<Calendar> calendars = calendarProvider.getCalendars().getList();

        return calendars;
    }

    public List<Event> getEventList(Calendar calendar){
        List<Event> events = calendarProvider.getEvents(calendar.id).getList();

        return events;
    }

}
