package com.example.filipe.myapplication;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class TaskLapse implements Serializable {
    private Date startedAt;
    private Date endedAt;

    public TaskLapse () {}

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    @Override
    public String toString() {
        return "TaskLapse{" +
                "startedAt=" + startedAt +
                ", endedAt=" + endedAt +
                '}';
    }

    public HashMap<String,Date> toFirebaseObject() {
        HashMap<String,Date> taskLapse =  new HashMap<String,Date>();

        taskLapse.put("startedAt", startedAt);
        taskLapse.put("endedAt", endedAt);

        return taskLapse;
    }

    public long getStartAtTime () {
        return (startedAt == null ? new Date() : startedAt).getTime();
    }

    public long getEndedAtTime () {
        return (endedAt == null ? new Date() : endedAt).getTime();
    }

    public long getMilliseconds() {
        long startAtTime = getStartAtTime();
        long endedAtTime = getEndedAtTime();

        return endedAtTime - startAtTime;
    }

    public boolean isToday () {
        long startOfDayTime = getStartOfDayInMillis();
        long endOfDayTime = getEndOfDayInMillis();
        long startAtTime = getStartAtTime();
        long endedAtTime = getEndedAtTime();

        return (startAtTime > startOfDayTime && endedAtTime < endOfDayTime);
    }

    public long getStartOfDayInMillis() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public long getEndOfDayInMillis() {
        return getStartOfDayInMillis() + (24 * 60 * 60 * 1000);
    }

}
