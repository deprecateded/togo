package com.example.filipe.myapplication.task;

import com.example.filipe.myapplication.Task;
import com.example.filipe.myapplication.account.Account;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;




public class TaskDAO {
    static FirebaseDatabase database;
    static DatabaseReference reference;

    public static FirebaseDatabase getInstance () {
        if (database != null) {
            return database;
        }
        database = FirebaseDatabase.getInstance();
        return database;
    }

    public static DatabaseReference getReference () {
        if (reference != null) {
            return  reference;
        }
        reference = getInstance().getReference(Account.getAccount().getId()).child("tasks");
        return reference;
    }

    public static DatabaseReference create (Task task) {
        DatabaseReference myRef = getReference();

        Map<String, Task> tasks = new HashMap<>();
        tasks.put(task.getTitleSlug(), task);

        myRef.setValue(tasks);

        return myRef;
    }


    public static DatabaseReference update (Task task) {
        DatabaseReference taskRef = getReference().child(task.getTitleSlug());
        taskRef.updateChildren(task.toFirebaseObject());
        return taskRef;
    }

    public static void delete(Task task) {
        DatabaseReference taskRef = getReference().child(task.getTitleSlug());
        taskRef.removeValue();
         }
}
