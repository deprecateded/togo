package com.example.filipe.myapplication;

import java.io.Serializable;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;

public class Task implements Serializable {
    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

    private String title;
    private Boolean running = true;
    private long milliseconds = 0;
    private TaskLapse currentLapse;
    private ArrayList<TaskLapse> lapses = new ArrayList<TaskLapse>();
    private String origin = "togo";

    public Task () {}

    public Task (String title) {
        this.title = title;
        startLapse();
    }

    public String getTitle() {
        return title;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public long getMilliseconds () {
        long sumMilliseconds = milliseconds;
        if (currentLapse != null && currentLapse.getStartedAt() != null) {
            Date now = new Date();
            sumMilliseconds += now.getTime() - currentLapse.getStartedAt().getTime();
        }
        return sumMilliseconds;
    }

    public long getTodayMilliseconds () {
        long total = 0;

        if (currentLapse != null) {
            total += currentLapse.getMilliseconds();
        }

        if (lapses.isEmpty()) {
            return total;
        }

        for (TaskLapse lapse: lapses) {
            if (lapse.isToday()) {
                total += lapse.getMilliseconds();
            }
        }

        return total;
    }

    public ArrayList<TaskLapse> getLapses() {
        return lapses;
    }

    public void startLapse () {
        currentLapse = new TaskLapse();
        currentLapse.setStartedAt(new Date());
    }

    public void stopLapse () {
        currentLapse.setEndedAt(new Date());
        lapses.add(currentLapse);
        currentLapse = null;
    }

    public void setMilliseconds (long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public Boolean isRunning () {
        return running;
    }

    public void toggle () {
        running = !running;
        if (running) {
            startLapse();
        } else {
            stopLapse();
        }
    }

    public HashMap<String,Object> toFirebaseObject() {
        HashMap<String,Object> task =  new HashMap<String,Object>();

        task.put("title", title);
        task.put("running", running);
        task.put("milliseconds", milliseconds);
        task.put("currentLapse", currentLapse);
        task.put("lapses", lapses);

        return task;
    }

    public String getTitleSlug() {
        String input = title;
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", running=" + running +
                ", milliseconds=" + milliseconds +
                ", currentLapse=" + currentLapse +
                ", lapses=" + lapses +
                '}';
    }

    public void setCurrentLapse(TaskLapse currentLapse) {
        this.currentLapse = currentLapse;
    }

    public void setLapses(ArrayList<TaskLapse> lapses) {
        this.lapses = lapses;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRunning(Boolean running) {
        this.running = running;
    }

    public TaskLapse getCurrentLapse() {
        return currentLapse;
    }
}
