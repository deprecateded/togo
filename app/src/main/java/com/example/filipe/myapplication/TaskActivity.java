package com.example.filipe.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.filipe.myapplication.task.TaskDAO;

public class TaskActivity extends AppCompatActivity {

    private static final String TAG = "TaskActivity";
    private TaskLapseAdapter mAdapter;
    private ListView listView;
    private Task task = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        final Intent intent = getIntent();
        task = (Task) intent.getSerializableExtra("task");
        TextView titleView = (TextView) findViewById(R.id.task_title);
        titleView.setText(task.getTitle());

        Button buttonView = (Button) findViewById(R.id.btn_excluir);
        buttonView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                TaskDAO.delete(task);
                finishActivity();
            }

        });

        if(task.getOrigin().equals("calendar")){
            buttonView.setVisibility(buttonView.GONE);
        }

        listView = (ListView) findViewById(R.id.task_lapses);

        updateUI();
    }

    private void finishActivity() {
        finish();
    }


    private void updateUI() {
        if (mAdapter == null) {
            Log.d(TAG, "Tasks: " + task.getLapses());
            mAdapter = new TaskLapseAdapter(this, task.getLapses());
            listView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }
    
}
