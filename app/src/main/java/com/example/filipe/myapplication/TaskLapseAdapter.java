package com.example.filipe.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TaskLapseAdapter extends ArrayAdapter<TaskLapse>
{

    private Context mContext;
    private ArrayList<TaskLapse> lapses = new ArrayList<>();


    public TaskLapseAdapter(@NonNull Context context, ArrayList<TaskLapse> lapses) {
        super(context, 0 , lapses);
        mContext = context;
        this.lapses = lapses;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.task_lapse,parent,false);

        final TaskLapse currentTaskLapse = lapses.get(position);

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        TextView startAt = (TextView) listItem.findViewById(R.id.task_lapse_start);
        startAt.setText(format.format(currentTaskLapse.getStartedAt()));
        TextView endAt = (TextView) listItem.findViewById(R.id.task_lapse_end);
        endAt.setText(format.format(currentTaskLapse.getEndedAt()));

        return listItem;
    }


}