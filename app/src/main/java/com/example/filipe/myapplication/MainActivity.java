package com.example.filipe.myapplication;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;
import com.example.filipe.myapplication.account.Account;
import com.example.filipe.myapplication.task.TaskDAO;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import me.everything.providers.android.calendar.Calendar;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int PERMISSIONS_REQUEST_READ_CALENDAR = 2;
    ListView listView;

    private TaskAdapter mAdapter = null;
    private ArrayList<Task> taskList = new ArrayList<Task>();
    private Pie pie = null;
    private ArrayList<DataEntry> pieData;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.list);
        listView.setAdapter(mAdapter);

        initGoogleClient();
        addChartRefreshButton();
        addFloatingActionButton();
    }

    private void checkCalendarPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CALENDAR}, PERMISSIONS_REQUEST_READ_CALENDAR);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
        else{
            loadCalendar();
        }
    }

    public void loadCalendar() {
        CalendarManager cm = new CalendarManager(this);
        Calendar userCalendar = null;

        for (Calendar c: cm.getCalendarList()) {
            if (c.displayName.equals(Account.getAccount().getEmail())) {
                userCalendar = c;
                break;
            }
        }

        if (userCalendar != null) {
            List<me.everything.providers.android.calendar.Event> el = cm.getEventList(userCalendar);
            for (me.everything.providers.android.calendar.Event e: el) {
                Log.i(TAG, e.toString());
                Boolean exists = false;
                for(Task t: taskList){
                    if(t.getTitle().equals(e.title)){
                        exists = true;
                        break;
                    }
                }
                if(exists == false){
                    Task task = new Task(e.title);
                    task.setOrigin("calendar");
                    Log.i(TAG, "criou task a partir do evento");
                    taskList.add(task);
                    TaskDAO.update(task);
                }
            }

            updateUI();

        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CALENDAR) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadCalendar();
            } else {
                Toast.makeText(this, "É necessário que permita a leitura do calendário para integrar", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initGoogleClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logoff:
                signOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void signOut() {
        final MainActivity ctx = this;
        mGoogleSignInClient.signOut()
        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                Account.setAccount(null);
                Intent intent = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(intent);
            }
        });
    }




    public void addChartRefreshButton() {
        FloatingActionButton button = findViewById(R.id.chart_refresh);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                refreshPieChart();
            }
        });
    }

    public void addFloatingActionButton () {
        FloatingActionButton button = findViewById(R.id.floatingActionButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addTask();
            }
        });
    }

    public void refreshPieChart () {
        AnyChartView anyChartView = findViewById(R.id.chart);

        if (pie == null) {
            Pie pie = AnyChart.pie();
            pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
                @Override
                public void onClick(Event event) {
                    Toast.makeText(MainActivity.this, event.getData().get("x") + ":" + event.getData().get("value"), Toast.LENGTH_SHORT).show();
                }
            });

            pie.legend()
                    .position("center-bottom")
                    .itemsLayout(LegendLayout.HORIZONTAL)
                    .align(Align.CENTER);

            this.pie = pie;
            anyChartView.setChart(this.pie);
        }

        List<DataEntry> data = new ArrayList<>();

        for (Task t: taskList) {
            Log.d(TAG, t.getTitle());
            data.add(new ValueDataEntry(t.getTitle(), t.getTodayMilliseconds()));
        }

        if (data.isEmpty()) {
            Log.d(TAG, "VAZIO");
            anyChartView.setVisibility(anyChartView.GONE);
        } else {
            Log.d(TAG, "COM COISA");
            anyChartView.setVisibility(anyChartView.VISIBLE);
            pie.data(data);
        }

        anyChartView.invalidate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        taskList.clear();
        addFirebaseListener();
    }

    private void addFirebaseListener() {
        ValueEventListener taskListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Task task = snapshot.getValue(Task.class);
                    taskList.add(task);
                }
                checkCalendarPermission();
                updateUI();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };

        TaskDAO.getReference().addListenerForSingleValueEvent(taskListener);
    }

    public void addTask () {
        showDialog();
    }

    private void updateUI() {
        if (mAdapter == null) {
            mAdapter = new TaskAdapter(this, taskList);
            listView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }

        refreshPieChart();
    }

    public void showDialog () {
        final EditText taskEditText = new EditText(this);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Adicionar nova tarefa")
                .setMessage("Qual o nome da tarefa?")
                .setView(taskEditText)
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String taskTitle = String.valueOf(taskEditText.getText());
                        if (!taskTitle.isEmpty()) {
                            boolean taskExists = false;
                            for (Task t: taskList) {
                                if (t.getTitle().equals(taskTitle)) {
                                    taskExists = true;
                                }
                            }
                            if (!taskExists) {
                                Task task = new Task(taskTitle);
                                TaskDAO.update(task);
                                taskList.add(task);
                                updateUI();
                            }

                        }
                    }
                })
                .setNegativeButton("Cancelar", null)
                .create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    public void toggleTask (View view) {
        updateUI();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

//    public String getGmailUser() {
//        AccountManager manager = AccountManager.get(this);
//        Account[] accounts = manager.getAccountsByType("com.google");
//        List<String> possibleEmails = new LinkedList<String>();
//
//        for (Account account : accounts) {
//            // TODO: Check possibleEmail against an email regex or treat
//            // account.name as an email address only for certain account.type values.
//            possibleEmails.add(account.name);
//        }
//
//        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
//            String email = possibleEmails.get(0);
//            String[] parts = email.split("@");
//
//            if (parts.length > 1)
//                return parts[0];
//        }
//        return "";
//    }
}
